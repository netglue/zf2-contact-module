# Netglue Generic Contact Form Module

First of all, this module was adapted from a module written by [Matthew Weier O'Phinney](http://mwop.net)
which you can find on github at [weierophinney/PhlyContact](https://github.com/weierophinney/PhlyContact)

The main differences is that the controller, on successful submission does nothing except trigger an event.
The reason for this is because I needed an excuse to learn a bit about the [ZF2 Event Manager](http://framework.zend.com/manual/2.0/en/modules/zend.event-manager.event-manager.html)
and I thought that it would be neat to be able to _plug in_ observers for various tasks that could accompany the submission of a contact form,
for example, detailed logging, storing messages to a database or file perhaps and of course beaming out email messages.

This is the first stand-alone module I've put together with ZF2 so sorry if it's a bit screwy.
It seems a bit configuration heavy to me and I'm not yet sure if that's just something I have to live with
or I'm doing things wrong!

##Installation

By far the easiest thing to do is install with [composer](http://getcomposer.org) by adding
`"netglue/ng-contact": "dev-master"` to the require block of your top-level `composer.json` file
then issue a `$ php composer.phar update` to pull in the module to your vendor directory _(Assuming
you've setup a standard ZF2 app like the Skeleton App)_.

Bear in mind that you'll probably also need to add `"minimum-stability":"dev"` to your top-level composer file as this and none of the package dependencies are 'stable'

This will also install the dependencies. Once installed, in the config directory is a file named
`module.netglue-contact.global.php.dist`. copy it to your `config/autoload` directory and 
chop the .dist off and alter to suit your needs _(There's a lot of cruft in the .dist config file that you will likely need to delete. A good starting point is to simply provide valid email addresses)_. The config files are reasonably well documented
and theoretically, it should work out of the box without any supplemental config, but you probably won't
like the view scripts and email templates much!
	
##Dependencies

* [Zend Framework](http://framework.zend.com)
* [Net Glue ZF Extensions](https://bitbucket.org/netglue/zf2). This is extra library code
  Specifically, the HtmlToText filter is required for plain text alternatives and the
  Mail Transport Factory is used to configure transports.
* [Net Glue Template Mail Module](https://bitbucket.org/netglue/zf2-mtmail-module). This little module
  provides an easy way to initialise multipart email messages that use the Zend View components to render
  the HTML

##TODO
 * Implement Captchas
 * Provide example configuration for altering the form to include additional fields
 * Possibly alter the observer to include help for the former
 * Add tests and clean up

