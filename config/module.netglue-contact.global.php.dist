<?php
/**
 * Examples for Overriding Configuration of the Netglue Contact Module
 *
 * It's good practice to leave the actual module well alone. You can control
 * pretty much every aspect of the module configuration from the context of
 * your applications top-level configuration directory. This would normally
 * be YourApp/config/autoload/
 * Simply copy this file to that directory and make sure it is named:
 *   module.netglue-contact.global.php (For your production Site)
 * And/or, for local development overrides,
 *   module.netglue-contact.local.php
 *
 * Make Sense? Took a while for me to get it all figured out :)
 *
 *
 */
return array(
	
	/**
	 * Override specific namespaced options for the netglue contact module
	 * 
	 * The options in the 'netglue_contact' array key, are specific to the
	 * operation of the module as opposed to general framework configuration
	 * For complete reference, take a look at ./module.config.php included
	 * with the distribution
	 */
	'netglue_contact' => array(
		
		/**
		 * Config that affects operation of the form
		 */
		'form' => array(
			'name' => 'contact',
			'attributes' => array(
				'class' => 'zend_form',
			),
			
			/**
			 * Override filter/validation settings
			 */
			'filter_config' => array(
				array(
					'name' => 'fromName',
					'validators' => array(
						// Add another validator if you fancy it!
						array(
							'name' => 'StringLength',
							'options' => array(
								'encoding' => 'UTF-8',
								'min' => 2,
								'max' => 140,
							),
						),
						// Change the message for the not-empty validator:
						array(
							'name' => 'NotEmpty',
							// $breakChainOnFailure is set to false when you override the defaults so make sure you set it true
							'break_chain_on_failure' => true,
							'options' => array(
								'messages' => array(
									// Find the message key in the validator class or refer to the constant
									'isEmpty' => 'Please provide your name so we know who we\'re talking to!',
									//\Zend\Validator\NotEmpty::IS_EMPTY => 'Come on dude. Type your name in!',
								),
							),
						),
					),
				),
			),
		),
		
		/**
		 * Config that affect the operation of the 'Responder' Observer class
		 * Most importantly, email sending options
		 */
		'responder' => array(
			'email' => array(
				'enable' => true, // Send Messages at all?
				'send_receipt' => true, // Send the customer autoresponse?
				'recipient' => 'Someone <me@example.com>', // Message receiver
				'sender' => 'Fred <fred@example.com>', // Send autoresponders as
				'template' => 'mail/contact-message', // Template to use. See main config for more info
				'receipt_template' => 'mail/receipt',
				'subject' => 'Website Contact',
				'receipt_subject' => 'Thank you for your message',
			),
		),
		
		/**
		 * Event Listeners/Observers that get attached to the SharedEventManager
		 * Only events that correspond to the NetglueContact\Controller\ContactController
		 * Can be registered
		 */
		'observers' => array(
			// Event Name => array(Of Callbacks)
			NetglueContact\Controller\ContactController::EVENT_RENDER_FORM => array(
				function(Zend\EventManager\EventInterface $e) {
					$name = $e->getName();
					$params = $e->getParams();
					// Will contain 1 value keyed 'form' containing the Form instance to render
					// ... Do Something ...
				},
				// Provide a static class method
				array('My\Observer\DoesSomething', 'inThisMethod'),
			),
			NetglueContact\Controller\ContactController::EVENT_CONTACT => array(
				function(Zend\EventManager\EventInterface $e) {
					$name = $e->getName();
					$params = $e->getParams();
					// Will contain 2 elements:
					// 'form' has the submitted form
					// 'data' contains the array of filtered data from $form->getData()
					// ... Do Something ...
				},
			),
		),
	),
	
	/**
	 * Change the base url of the contact controller's index action
	 * You can override as much or as little of the route configuration
	 * as you like, but only the base route is shown below for brevity
	 */
	'router' => array(
		'routes' => array(
			'contact_form' => array(
				'options' => array(
					'route' => '/someplace-else',
				),
			),
		),
	),
	
	
	/**
	 * Don't like the form layout in the module? or want to customise the view
	 * script completely?
	 * 
	 * Create a directory somewhere to hold the view scripts, or use the directory
	 * that you already have for your 'default' module.
	 * 
	 * We'll assume your default module is called 'Application' and view scripts
	 * are in the 'view' directory.
	 * The path you enter should be relative to the location of this config file,
	 * which should be in your main app-wide config directory
	 * The path we end up with work out to:
	 *   YourApp/module/Application/view
	 * Inside your views should be the directory structure:
	 *   /netglue-contact/contact/view-script.phtml
	 * where view-script.phtml corresponds to each controller action that is available.
	 */
	'view_manager' => array(
		'template_path_stack' => array(
			'netglue-contact' => __DIR__ . '/../../module/Application/view', // Files in this directory are divided by /module-name/controller-name/action-name.phtml (Drop the 'Controller' part - i.e. FooController becomes 'foo')
		),
	),
	
	
);