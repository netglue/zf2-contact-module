<?php
/**
 * Netglue Contact Module Form Class
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 * @package	Netglue_ContactModule
 * @link https://bitbucket.org/netglue/zf2-contact-module
 */
 
namespace NetglueContact\Form;

use Zend\Captcha\AdapterInterface as CaptchaAdapter;
use Zend\Form\Element;
use Zend\Form\Form;

/**
 * Netglue Contact Module Form Class
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 * @package	Netglue_ContactModule
 * @link https://bitbucket.org/netglue/zf2-contact-module
 */
class ContactForm extends Form {
	
	/**
	 * Captcha Adapter Interface or NULL if disabled
	 * @var Zend\Captcha\AdapterInterface|NULL
	 */
	protected $captchaAdapter;
	
	protected $csrfToken;
	
	/**
	 * Construct
	 * @param string $name Name of form
	 * @param Zend\Captcha\AdapterInterface $captchaAdapter
	 * @return void
	 */
	public function __construct($name = null, CaptchaAdapter $captchaAdapter = null) {
		parent::__construct($name);
		if(null !== $captchaAdapter) {
			$this->captchaAdapter = $captchaAdapter;
		}
		$this->init();
	}
	
	/**
	 * Init Form
	 * @return void
	 */
	public function init() {
		/**
		 * Provide a name for the form if not already set
		 */
		$name = $this->getName();
		if(null === $name) {
			$this->setName('contact');
		}
		
		$name = new \Zend\Form\Element\Text('fromName');
		$name->setLabel('Your Name');
		$name->setAttributes(array(
			'size' => 60,
			'required' => 'required',
			'maxlength' => 255,
			'title' => 'Please provide your name so we know who we\'re talking to',
			'id' => 'fromName',
		));
		$this->add($name);
		
		$email = new \Zend\Form\Element\Email('email');
		$email->setLabel('Email Address');
		$email->setAttributes(array(
			'placeholder' => 'you@example.com',
			'required' => 'required',
			'title' => 'Please provide an email address so that we can respond to your message',
			'id' => 'email',
		));
		$this->add($email);
		
		$body = new \Zend\Form\Element\Textarea('message');
		$body->setLabel('Your message')
			->setAttributes(array(
				'required' => 'required',
				'rows' => 15,
				'title' => 'Don\'t forget to write something here...',
				'id' => 'message',
			));
		$this->add($body);
		
		if(NULL !== $this->captchaAdapter) {
			$captcha = new Element\Captcha('captcha');
			$captcha->setCaptcha($this->captchaAdapter);
			$captcha->setOptions(array('label' => 'Please verify you are human.'));
			$captcha->setAttributes(array('id' => 'captcha'));
			$this->add($captcha);
		}
		
		/*$csrf = new Element\Csrf('csrf', array('csrf_options' => array('timeout' => 3600)));
		$this->add($csrf);*/
		
		$submit = new \Zend\Form\Element\Submit('submitContact');
		$submit->setValue('Send Message');
		$this->add($submit);
		
	}
}
