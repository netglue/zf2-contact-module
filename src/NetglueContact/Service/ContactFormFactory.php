<?php
/**
 * A Factory to create a contact form instance
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 * @package	Netglue_ContactModule
 * @link https://bitbucket.org/netglue/zf2-contact-module
 */

namespace NetglueContact\Service;

use Traversable;
use NetglueContact\Form\ContactFilter;
use NetglueContact\Form\ContactForm;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\ArrayUtils;

/**
 * A Factory to create a contact form instance
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 * @package	Netglue_ContactModule
 * @link https://bitbucket.org/netglue/zf2-contact-module
 */
class ContactFormFactory implements FactoryInterface {
	
	/**
	 * Return our contact form
	 * @param ServiceLocatorInterface $services
	 * @return NetglueContact\Form\ContactForm
	 */
	public function createService(ServiceLocatorInterface $serviceLocator) {
		$config  = $serviceLocator->get('config');
		if($config instanceof Traversable) {
			$config = ArrayUtils::iteratorToArray($config);
		}
		
		
		$config = isset($config['netglue_contact']['form']) ? $config['netglue_contact']['form'] : array();
		
		$captcha = NULL;
		if(isset($config['captcha']['enable']) && false !== $config['captcha']['enable']) {
			$captcha = $serviceLocator->get('NetglueContactCaptcha');
		}
		
		$formName = isset($config['name']) ? $config['name'] : 'contact';
		$atrs = isset($config['attributes']) ? $config['attributes'] : array();
		
		$form = new ContactForm($formName, $captcha);
		
		foreach($atrs as $name => $value) {
			$form->setAttribute($name, $value);
		}
		
		$filter = new ContactFilter();
		$form->setInputFilter($filter);
		
		if(isset($config['filter_config'])) {
			foreach($config['filter_config'] as $f) {
				$filter->add($f);
			}
		}
		return $form;
	}
	
}
